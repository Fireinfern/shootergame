using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Controllers;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Damage;
using GameMode;

// class to retrieve HealthComponent of Player and display as health bar
// if HealthComponent of the Player does not change, HealthBarDisplay has its own logic
// to change the bar length. But if HealthComponent changes, HealthBarDisplay will follow
// the values of HealthComponent

public class HealthBarDisplay : MonoBehaviour
{
    public Image meterImage;
    public Text meterText;
    public float healthToZeroInSeconds = 60;

    private float initialHealth = 100; // some dummy values
    private float currentHealth = 100; // some dummy values
    private float maxHealth = 100; // some dummy values
    private float lastCurrentHealth = 100; // some dummy values

    private HealthComponent healthComponent = null; // to store healthComponent of the Player
    private GameObject playerObject = null; // to get the Player object

    private void Start()
    {
        GameplayManager gameplayManager = GameplayManager.Instance;
        if (gameplayManager.PlayerController)
        {
            playerObject = gameplayManager.PlayerController.GetComponent<PlayerController>()?.ControlledCharacter;
            if (!playerObject)
            {
                return;
            }
            healthComponent = playerObject.GetComponent<HealthComponent>();
            if (healthComponent)
            {
                maxHealth = healthComponent.MaxHealth;
                initialHealth = healthComponent.InitialHealth;
                currentHealth = healthComponent.CurrentHealth;
            }
        }
        gameplayManager.OnControlledCreated.AddListener(OnControllerCreated);
    }

    void Update()
    {
        if (!playerObject) return;

        // if healthComponent.CurrentHealth changed, follow healthComponent.CurrentHealth
        if (lastCurrentHealth != healthComponent.CurrentHealth)
        {
            currentHealth = healthComponent.CurrentHealth;
            lastCurrentHealth = currentHealth;
        }
        else // otherwise, decrease currentHealth, for display purpose
        {
            currentHealth -= maxHealth / healthToZeroInSeconds * Time.deltaTime;
        }

        // calculate values to display
        meterImage.fillAmount = (currentHealth < 0 ? 0 : currentHealth / maxHealth);
        // if currentHealth drops to zero, game over
        if (meterImage.fillAmount <= 0)
        {
            enabled = false;
            SceneManager.LoadScene("LostMenu", LoadSceneMode.Additive);
        }
        // update meter text
        meterText.text = "" + (int)(meterImage.fillAmount * 100);
    }

    void OnControllerCreated(GameObject controller, GameObject character)
    {
        playerObject = character;
        healthComponent = playerObject.GetComponent<HealthComponent>();
        if (healthComponent)
        {
            maxHealth = healthComponent.MaxHealth;
            initialHealth = healthComponent.InitialHealth;
            currentHealth = healthComponent.CurrentHealth;
        }
    }
}
