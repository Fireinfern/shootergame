using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Controllers;

[System.Serializable]
public class TransformData
{
    public float posX;
    public float posY;
    public float posZ;

    public float rotX;
    public float rotY;
    public float rotZ;
    public float rotW;
}

public class SaveLoadManager : MonoBehaviour
{
    private string saveFilePath;
    public TransformData PlayerStartData { get; set; }
    public static SaveLoadManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        saveFilePath = Path.Combine(Application.persistentDataPath, "savegame.dat");
    }

    public void SaveGame()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        var camera = GameObject.Find("Camera");
        var playerStartBase = GameObject.Find("PlayerStartBase");
        
        var saveData = new Dictionary<string, TransformData>();

        if (player)
        {
            PlayerStartData = GetTransformData(player.transform);

            saveData["Player"] = GetTransformData(player.transform);

            Debug.Log(
                $"Saved Player position {player.transform.position} and rotation {player.transform.rotation.eulerAngles}"
            );
        }

        // if (camera)
        // {
        //     saveData["Camera"] = GetTransformData(camera.transform);
        //     Debug.Log(
        //         $"Saved Camera position {camera.transform.position} and rotation {camera.transform.rotation.eulerAngles}"
        //     );
        // }

        // // Saving the PlayerStartBase object's transform
        // if (playerStartBase)
        // {
        //     saveData["PlayerStartBase"] = GetTransformData(playerStartBase.transform);
        //     Debug.Log(
        //         $"Saved PlayerStartBase position {playerStartBase.transform.position} and rotation {playerStartBase.transform.rotation.eulerAngles}"
        //     );
        // }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(saveFilePath);
        bf.Serialize(file, saveData);
        file.Close();
    }

    public void LoadGame()
    {
        if (File.Exists(saveFilePath))
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            var camera = GameObject.Find("Camera");
            var playerStartBase = GameObject.Find("PlayerStartBase"); // Finding the PlayerStartBase object

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(saveFilePath, FileMode.Open);
            var loadData = (Dictionary<string, TransformData>)bf.Deserialize(file);
            file.Close();

            if (loadData.ContainsKey("Player"))
            {
                // SetTransformData(player.transform, loadData["Player"]);
                // PlayerStartData = GetTransformData(player.transform);
                PlayerStartData = loadData["Player"];
                Debug.Log(
                    $"Loaded Player position {PlayerStartData}"
                );
            }

            // if (loadData.ContainsKey("Camera") && camera)
            // {
            //     SetTransformData(camera.transform, loadData["Camera"]);
            //     Debug.Log(
            //         $"Loaded Camera position {camera.transform.position} and rotation {camera.transform.rotation.eulerAngles}"
            //     );
            // }

            // // Loading the PlayerStartBase object's transform
            // if (loadData.ContainsKey("PlayerStartBase") && playerStartBase)
            // {
            //     SetTransformData(playerStartBase.transform, loadData["PlayerStartBase"]);
            //     Debug.Log(
            //         $"Loaded PlayerStartBase position {playerStartBase.transform.position} and rotation {playerStartBase.transform.rotation.eulerAngles}"
            //     );
            // }
        }
    }

    private TransformData GetTransformData(Transform transform)
    {
        return new TransformData
        {
            posX = transform.position.x,
            posY = transform.position.y,
            posZ = transform.position.z,
            rotX = transform.rotation.x,
            rotY = transform.rotation.y,
            rotZ = transform.rotation.z,
            rotW = transform.rotation.w
        };
    }

    // private void SetTransformData(Transform transform, TransformData transformData)
    // {
    //     Vector3 position = new Vector3(transformData.posX, transformData.posY, transformData.posZ);
    //     Quaternion rotation = new Quaternion(
    //         transformData.rotX,
    //         transformData.rotY,
    //         transformData.rotZ,
    //         transformData.rotW
    //     );


    // }
}
