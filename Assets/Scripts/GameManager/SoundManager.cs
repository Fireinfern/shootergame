﻿using System;
using UnityEngine;

namespace GameManager
{
    public class SoundManager : MonoBehaviour
    {
        private static SoundManager _instance;

        public SoundManager Instance => _instance;

        private float _volume = 1.0f;

        public float Volume => _volume;

        private AudioSource _musicSource;
        
        private void Awake()
        {
            if (!_instance)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
                return;
            }
            Destroy(this);
        }

        private void Start()
        {
            _musicSource = GetComponent<AudioSource>();
        }

        void SetVolume(float newVolume)
        {
            _volume = Mathf.Clamp(newVolume, 0.0f, 1.0f);
        }
    }
}