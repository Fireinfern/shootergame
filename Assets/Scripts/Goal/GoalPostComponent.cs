﻿using System;
using Interactions;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Goal
{
    [RequireComponent(typeof(SimpleTriggerInteraction))]
    public class GoalPostComponent : MonoBehaviour
    {
        private SimpleTriggerInteraction _simpleTriggerInteraction;

        private void Start()
        {
            _simpleTriggerInteraction = GetComponent<SimpleTriggerInteraction>();
            _simpleTriggerInteraction.TriggerEntered.AddListener(TriggerEntered);
        }

        private void TriggerEntered(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            SceneManager.LoadScene("WinMenu", LoadSceneMode.Additive);
        }
    }
}