﻿using System;
using Registry;
using UnityEngine;

namespace PlayerStart
{
    public class PlayerStart : MonoBehaviour
    {

        [SerializeField]
        private String playerStartName = "";

        public String PlayerStartName => playerStartName;
    }
}