﻿using System;
using System.Collections.Generic;
using Damage;
using UnityEngine;

namespace Enemies
{
    public class Detection : MonoBehaviour
    {

        [SerializeField]
        private float timeToKill = 2.0f;

        private float _timeLeftToKill = 0.0f;

        [SerializeField]
        private float targetExtraRotation = -90.0f;

        private Quaternion _targetRotation;

        private Quaternion _initialRotation;

        private GameObject _targetedPlayer;

        [SerializeField]
        private float rotationSpeed = 3.0f;
        
        private float _rotationRate = 1.0f;

        private bool _justChangeDirections = false;

        private void Start()
        {
            _initialRotation = transform.rotation;
            _targetRotation = _initialRotation * Quaternion.Euler(0.0f, targetExtraRotation, 0.0f);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            _timeLeftToKill = timeToKill;
            _targetedPlayer = other.gameObject;
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            _targetedPlayer = null;
        }

        private void Update()
        {
            if (_timeLeftToKill <= 0.0f)
            {
                Quaternion currentTarget = _rotationRate > 0.0f ? _targetRotation : _initialRotation;
                transform.rotation = Quaternion.Lerp(transform.rotation, currentTarget, rotationSpeed * Time.deltaTime);
                if (!_justChangeDirections &&
                    (transform.rotation == currentTarget))
                {
                    _rotationRate *= -1.0f;
                    _justChangeDirections = true;
                }
                if (_justChangeDirections) _justChangeDirections = false;
                return;
            }
            _timeLeftToKill -= Time.deltaTime;
            if (_timeLeftToKill <= 0.0f)
            {
                _timeLeftToKill = 0.0f;
                ApplyDamage();    
            }
        }

        private void ApplyDamage()
        {
            if (!_targetedPlayer) return;
            _targetedPlayer.GetComponent<HealthComponent>()?.IReceiveDamage(2000.0f);
        }
    }
}