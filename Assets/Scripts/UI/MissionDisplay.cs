﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class MissionDisplay : MonoBehaviour
    {
        [SerializeField] private TMP_Text missionTitleText;

        [SerializeField] private TMP_Text missionDescriptionText;

        public void ChangeMission(string missionTitle, string missionDescription)
        {
            missionTitleText.SetText(missionTitle);
            missionDescriptionText.SetText(missionDescription);
        }
    }
}