﻿using System;
using GameMode;
using UnityEngine;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        private static UIManager _instance;

        public static UIManager Instance => _instance;

        [SerializeField]
        private GameObject pauseMenu;
        
        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }

            _instance = this;
        }

        public void OpenPauseMenu()
        {
            GameplayManager.Instance.PauseGame();
            pauseMenu.SetActive(true);
        }

        public void ClosePauseMenu()
        {
            GameplayManager.Instance.ResumeGame();
            pauseMenu.SetActive(false);
        }
    }
}