﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject settingsPanel;

        [SerializeField] private GameObject mainMenuPanel;

        public void OpenSettingsMenu()
        {
            mainMenuPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }

        public void OpenMainMenu()
        {
            mainMenuPanel.SetActive(true);
            settingsPanel.SetActive(false);
        }

        public void StartNewGame()
        {
            SaveLoadManager.Instance.PlayerStartData = null;
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        }
 
         // yyip8
        public void LoadSavedGame()
        {
            SaveLoadManager.Instance.LoadGame();
//             SceneManager.LoadScene("MainMenu", LoadSceneMode.Single); // trick to properly reload the SampleScene
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        }

        // end yyip8

 
    }
}