﻿using Characters;
using Controllers;
using GameMode;
using Interactions;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class InventoryUIController : MonoBehaviour
    {
        [SerializeField]
        private Text keyCounterText;

        private int _keyCount = 0;

        private void Start()
        {
            GameplayManager gameplayManager = GameplayManager.Instance;
            if (gameplayManager.PlayerController)
            {
                GameObject playerObject = gameplayManager.PlayerController.GetComponent<PlayerController>()?.ControlledCharacter;
                if (!playerObject)
                {
                    return;
                }
                var inventory = playerObject.GetComponent<InventoryComponent>();
                if (inventory)
                {
                    inventory.onItemCollected.AddListener(OnItemCollected);
                    if (inventory.PlayerInventory.TryGetValue("key", out var value))
                    {
                        _keyCount = value;
                        keyCounterText.text = "Keys: " + _keyCount;
                    }
                }
            }
            gameplayManager.OnControlledCreated.AddListener(OnControllerCreated);
        }
        
        
        private void OnItemCollected(CollectableItem item)
        {
            if (!item.itemName.Equals("key"))
            {
                return;
            }

            _keyCount += item.amount;
            keyCounterText.text = "Keys: " + _keyCount;
        }

        private void OnControllerCreated(GameObject playerController, GameObject character)
        {
            character.GetComponent<InventoryComponent>()?.onItemCollected.AddListener(OnItemCollected);
        }
    }
}