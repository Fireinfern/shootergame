﻿using System;
using Missions;
using UnityEngine;
using System.Collections.Generic;

namespace UI
{
    public class MissionController : MonoBehaviour
    {
        private static MissionController _instance;

        public static MissionController Instance => Instance;

        [SerializeField]
        private List<MissionComponent> registeredMissions;

        [SerializeField]
        private MissionDisplay missionDisplay;

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }
            _instance = this;
        }

        private void Start()
        {
            Mission firstMission = registeredMissions[0].AssignedMission;
            missionDisplay.ChangeMission(firstMission.missionTitle, firstMission.missionDescription);
            foreach (var mission in registeredMissions)
            {
                mission.missionCompleted.AddListener(OnMissionCompleted);
            }
        }

        private void OnMissionCompleted(Mission mission)
        {
            registeredMissions.RemoveAll((missionComp) => mission != null && mission == missionComp.AssignedMission);
            if (registeredMissions.Count <= 0)
            {
                missionDisplay.ChangeMission("", "");
                return;
            }
            Mission firstMission = registeredMissions[0].AssignedMission;
            missionDisplay.ChangeMission(firstMission.missionTitle, firstMission.missionDescription);
        }
    }
}