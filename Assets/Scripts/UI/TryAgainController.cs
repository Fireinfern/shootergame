﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class TryAgainController : MonoBehaviour
    {
        public void OnTryAgain()
        {
            Scene scene = SceneManager.GetActiveScene();
            if (scene.name=="SampleScene"){
             SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);   
            }
            if (scene.name=="extralevel"){
             SceneManager.LoadScene("extralevel", LoadSceneMode.Single);   
            }
            
        }

        public void OnExit()
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}