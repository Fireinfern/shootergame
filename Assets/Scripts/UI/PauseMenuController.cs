﻿using System;
using GameMode;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class PauseMenuController : MonoBehaviour
    {
        private void OnEnable()
        {
            // TODO: ReproduceAnimation
        }

        public void ResumeGame()
        {
            GameplayManager.Instance.ResumeGame();
            gameObject.SetActive(false);
        }

        public void ExitGame()
        {
            Time.timeScale = 1.0f;
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }

        // yyip8
        public void SaveGame()
        {
            SaveLoadManager.Instance.SaveGame();
            GameplayManager.Instance.ResumeGame();
            gameObject.SetActive(false);
        }

        public void LoadGame()
        {
            SaveLoadManager.Instance.LoadGame();
            Time.timeScale = 1.0f;                  // these three lines have to exist to properly reload the SampleScene
            GameplayManager.Instance.ResumeGame(); // these three lines have to exist to properly reload the SampleScene
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single); // these three lines have to exist to properly reload the SampleScene
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
            gameObject.SetActive(false);
        }

        // end yyip8
    }
}
