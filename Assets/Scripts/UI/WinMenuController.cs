﻿using System;
using GameMode;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class WinMenuController : MonoBehaviour
    {
        private void Start()
        {   
            GameplayManager.Instance.PauseGame();
        }

        public void OpenMainMenu()
        {
            GameplayManager.Instance.ResumeGame();
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }

        public void Continue()
        {
            GameplayManager.Instance.ResumeGame();
            // For now both buttons will send you to the main menu
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}