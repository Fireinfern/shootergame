﻿using UnityEngine;

namespace Missions
{
    [CreateAssetMenu(fileName = "NewMission", menuName = "Mission", order = 0)]
    public class Mission : ScriptableObject
    {
        public string missionTitle;

        public string missionDescription;
    }
}