﻿using UnityEngine;
using UnityEngine.Events;

namespace Missions
{
    public class MissionComponent : MonoBehaviour
    {
        [SerializeField] private Mission mission;

        public Mission AssignedMission => mission;

        public UnityEvent<Mission> missionCompleted;

        public void CompleteMission()
        {
            missionCompleted.Invoke(mission);
        }
    }
}