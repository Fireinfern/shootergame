﻿using System;
using System.Collections.Generic;
using Controllers;
using Registry;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;
using UnityEngine.SceneManagement;

namespace GameMode
{
    public class GameplayManager : MonoBehaviour
    {
        static public GameplayManager Instance { get; private set; }

        [SerializeField]
        private GameMode gameMode;

        private GameObject _playerController;

        public GameObject PlayerController => _playerController;

        public GameMode GameMode => gameMode;

        public UnityEvent<GameObject, GameObject> OnControlledCreated;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        private void Start()
        {
            if (GameMode == null || !GameMode.IsValid())
                return;

            LevelRegistrySubsystem levelRegistrySubsystem = LevelRegistrySubsystem.Instance;
            PlayerStart.PlayerStart[] playerStarts =
                Object.FindObjectsOfType<PlayerStart.PlayerStart>();
            if (playerStarts.Length < 1)
                return;
            PlayerStart.PlayerStart selectedStart = playerStarts[0];
            foreach (var start in playerStarts)
            {
                if (start.PlayerStartName.Equals(GameMode.PlayerStartName))
                {
                    selectedStart = start;
                    break;
                }
            }

            Transform startTransform = selectedStart.transform;

            // yyip8

            // Check if there's a saved transform data for PlayerStartBase
            var saveLoadManager = SaveLoadManager.Instance;
            if (saveLoadManager && saveLoadManager.PlayerStartData != null)
            {
                startTransform.position = new Vector3(
                    saveLoadManager.PlayerStartData.posX,
                    saveLoadManager.PlayerStartData.posY,
                    saveLoadManager.PlayerStartData.posZ
                );
                startTransform.rotation = new Quaternion(
                    saveLoadManager.PlayerStartData.rotX,
                    saveLoadManager.PlayerStartData.rotY,
                    saveLoadManager.PlayerStartData.rotZ,
                    saveLoadManager.PlayerStartData.rotW
                );
            }

            // end yyip8

            _playerController = Instantiate(
                GameMode.ControllerPrefab,
                startTransform.position,
                startTransform.rotation
            );
            GameObject character = Instantiate(
                GameMode.CharacterPrefab,
                startTransform.position,
                startTransform.rotation
            );
            _playerController.GetComponent<IControllerInterface>()?.IPossess(character);
            OnControlledCreated.Invoke(_playerController, character);
        }

        public void PauseGame()
        {
            Time.timeScale = 0.0f;
        }

        public void ResumeGame()
        {
            Time.timeScale = 1.0f;
        }
    }
}
