﻿namespace Damage
{
    public interface IDamageReceiverInterface
    {
        void IReceiveDamage(float damage);

        void IReceiveHeal(float healing);
        
    }
}