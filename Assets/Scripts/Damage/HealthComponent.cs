﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Damage
{
    public class HealthComponent : MonoBehaviour, IDamageReceiverInterface
    {
        [SerializeField] private float maxHealth = 100.0f;

        [SerializeField] private float initialHealth = 100.0f;

        [SerializeField] private float currentHealth = 100.0f;
        
        public float MaxHealth => maxHealth;

        public float InitialHealth => initialHealth;

        public float CurrentHealth => currentHealth;

        public UnityEvent<float, float> healthChanged;

        public void IReceiveDamage(float damage)
        {
            damage = Mathf.Min(damage, -damage);
            float beforeUpdateHealth = currentHealth;
            currentHealth += damage;
            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
            if (Mathf.Approximately(currentHealth, beforeUpdateHealth)) return;
            float newHealth = currentHealth;
            healthChanged.Invoke(initialHealth, newHealth);
            if (newHealth > 0.0f) return;
            SceneManager.LoadScene("LostMenu", LoadSceneMode.Additive);
        }

        public void IReceiveHeal(float healing)
        {
            healing = Mathf.Max(healing, -healing);
            float beforeUpdateHealth = currentHealth;
            currentHealth = Mathf.Clamp(currentHealth + healing, 0, maxHealth);
            if (Mathf.Approximately(currentHealth, beforeUpdateHealth)) return;
            float newHealth = currentHealth;
            healthChanged.Invoke(initialHealth, newHealth);
        }
    }
}