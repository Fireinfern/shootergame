﻿using System;
using UnityEngine;

namespace Damage
{
    public class HazardComponent : MonoBehaviour
    {

        [SerializeField] private DamageObject damageObject;
        
        private void OnTriggerEnter(Collider other)
        {
            HealthComponent healthComponent = other.GetComponent<HealthComponent>();
            if (!healthComponent) return;
            healthComponent.IReceiveDamage(damageObject.DamageAmount);
        }
    }
}