﻿using System;
using UnityEngine;

namespace PlayerCamera
{
    public class CamaraController : MonoBehaviour
    {
        [SerializeField] private Transform targetTransform;

        private void FixedUpdate()
        {
            Vector3 currentPosition = transform.position;
            transform.rotation = targetTransform.rotation;
            transform.position = Vector3.Lerp(currentPosition, targetTransform.position, 0.3f);
            // RaycastHit hit;
            // if (Physics.SphereCast(transform.position, 3.0f, Vector3.zero, out hit))
            // {
            //     Vector3 location = hit.transform.position - transform.position;
            //     transform.position = transform.position + location;
            // }
        }
    }
}