﻿using Characters;
using UnityEngine;

namespace Interactions
{
    public class ItemGiverComponent : MonoBehaviour
    {
        [SerializeField]
        private CollectableItem collectableItem;
        
        public void GiveItem(Collider other)
        {
            other.GetComponent<InventoryComponent>()?.AddItemToInventory(collectableItem);
            Destroy(gameObject);
        }
    }
}