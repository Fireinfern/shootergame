﻿using UnityEngine;

namespace Interactions
{
    [CreateAssetMenu(fileName = "Item", menuName = "Collectible", order = 0)]
    public class CollectableItem : ScriptableObject
    {
        public string itemName = "Item";

        public int amount = 1;
    }
}