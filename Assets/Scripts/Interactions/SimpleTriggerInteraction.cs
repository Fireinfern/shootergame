﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Interactions
{
    public class SimpleTriggerInteraction : MonoBehaviour
    {
        public UnityEvent<Collider> TriggerEntered;
        
        private void OnTriggerEnter(Collider other)
        {
            TriggerEntered.Invoke(other);
        }
    }
}