﻿using UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class UIController : MonoBehaviour, IInputBindedInterface
    {

        private bool _isGamePaused = false;
        
        public void ISetupInputBindings(PlayerInput inputComponent)
        {
            InputAction pauseAction = inputComponent.actions.FindAction("Pause");
            if (pauseAction != null)
            {
                pauseAction.performed += PauseGame;
            }
        }

        private void PauseGame(InputAction.CallbackContext context)
        {
            if (_isGamePaused)
            {
                UIManager.Instance.ClosePauseMenu();
                return;
            }
            UIManager.Instance.OpenPauseMenu();
        }
        
        
    }
}