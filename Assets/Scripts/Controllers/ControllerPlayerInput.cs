﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class ControllerPlayerInput : PlayerInput
    {
        private void Start()
        {
            IInputBindedInterface[] components = GetComponents<IInputBindedInterface>();
            foreach (var inputComponent in components)
            {
                inputComponent.ISetupInputBindings(this);
            }
        }
    }
}