﻿using System;
using Characters;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    [RequireComponent(typeof(PlayerInput))]
    public class MovementController
        : MonoBehaviour,
            IInputBindedInterface,
            IControllerComponentInterface
    {
        private Vector2 _movementVector = Vector2.zero;

        private Vector2 _cameraVector = Vector2.zero;

        private GameObject _possessedObject;

        private MovementComponent _possessedMovementComponent;

        private RotationComponent _possessedRotationComponent;

        private JumpComponent _possessedJumpComponent;

        public void ISetupInputBindings(PlayerInput inputComponent)
        {
            if (inputComponent == null)
                return;
            InputAction action = inputComponent.actions.FindAction("Move", true);
            action.performed += MoveActionPerformed;
            action.canceled += MoveActionCanceled;
            InputAction lookAction = inputComponent.actions.FindAction("Look", true);
            lookAction.performed += LookPerformed;
            InputAction jumpAction = inputComponent.actions.FindAction("Jump", true);
            jumpAction.performed += JumpPerformed;
        }

        void MoveActionPerformed(InputAction.CallbackContext context)
        {
            _movementVector = context.ReadValue<Vector2>();
            _movementVector.Normalize();
        }

        void MoveActionCanceled(InputAction.CallbackContext context)
        {
            _movementVector = context.ReadValue<Vector2>();
            _movementVector.Normalize();
        }

        void LookPerformed(InputAction.CallbackContext context)
        {
            _cameraVector = context.ReadValue<Vector2>();
            _cameraVector.Normalize();
            if (!_possessedRotationComponent)
                return;

            _possessedRotationComponent.AddInput(_cameraVector);
        }

        void JumpPerformed(InputAction.CallbackContext context)
        {
            if (!_possessedJumpComponent)
                return;
            _possessedJumpComponent.Jump();
        }

        private void FixedUpdate()
        {
            if (_possessedMovementComponent)
            {
                _possessedMovementComponent.AddInput(_movementVector);
            }
        }

        public void IOnPosses(GameObject possessedObject)
        {
            if (possessedObject == null)
                return;
            _possessedObject = possessedObject;
            _possessedMovementComponent = _possessedObject.GetComponent<MovementComponent>();
            _possessedRotationComponent = _possessedObject.GetComponent<RotationComponent>();
            _possessedJumpComponent = _possessedObject.GetComponent<JumpComponent>();
        }

        public void IOnUnPossess()
        {
            _possessedObject = null;
            _possessedMovementComponent = null;
            _possessedRotationComponent = null;
            _possessedJumpComponent = null;
        }
    }
}
