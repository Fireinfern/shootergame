﻿using UnityEngine.InputSystem;

namespace Controllers
{
    public interface IInputBindedInterface
    {
        void ISetupInputBindings(PlayerInput inputComponent);
    }
}