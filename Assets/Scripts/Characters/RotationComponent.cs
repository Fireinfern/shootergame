﻿using System;
using UnityEngine;

namespace Characters
{
    public class RotationComponent : MonoBehaviour
    {
        [SerializeField] private Camera _camera;

        [SerializeField] private GameObject controlRotator;

        private Vector2 _inputRotation = new Vector2(0.0f, 0.0f);

        private Quaternion _targetRotation;
        
        public GameObject ControlRotator => controlRotator;

        [SerializeField]
        private float rotationSpeed = 10.0f;

        private void Start()
        {
            if (!_camera) _camera = Camera.current;
        }

        private void FixedUpdate()
        {
            controlRotator.transform.localRotation = Quaternion.Lerp(controlRotator.transform.localRotation, _targetRotation, 0.2f);
        }

        public void AddInput(Vector2 cameraInput)
        {
            _inputRotation += cameraInput * rotationSpeed * Time.fixedDeltaTime;
            _inputRotation.y = Mathf.Clamp(_inputRotation.y, -70.0f, 70.0f);
            _targetRotation = Quaternion.Euler(_inputRotation.y, _inputRotation.x, 0.0f);
        }

        public Quaternion GetHorizontalRotationQuaternion()
        {
            return Quaternion.Euler(0.0f, _inputRotation.x, 0.0f);
        }
    }
}