﻿using System;
using System.Collections.Generic;
using Interactions;
using UnityEngine;
using UnityEngine.Events;

namespace Characters
{
    public class InventoryComponent : MonoBehaviour
    {
        private Dictionary<String, int> _playerInventory = new Dictionary<string, int>();

        public Dictionary<String, int> PlayerInventory => _playerInventory = new Dictionary<string, int>();
    
        public UnityEvent<CollectableItem> onItemCollected;

        public void AddItemToInventory(CollectableItem item)
        {
            onItemCollected.Invoke(item);
            if (_playerInventory.ContainsKey(item.itemName))
            {
                _playerInventory[item.itemName] += item.amount;
                return;
            }
            _playerInventory.Add(item.itemName, item.amount);
        }
        
        
    }
}