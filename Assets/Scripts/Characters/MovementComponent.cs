﻿using System;
using System.Numerics;
using Cinemachine;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Characters
{
    [RequireComponent(typeof(CharacterController)), RequireComponent(typeof(RotationComponent))]
    public class MovementComponent : MonoBehaviour
    {
        private CharacterController _characterController;

        private RotationComponent _rotationComponent;

        private JumpComponent _jumpComponent;

        [SerializeField]
        private float speed = 10.0f;

        [SerializeField]
        private bool hasToRotateWithCamera = false;

        [SerializeField]
        private GameObject characterMesh;

        [SerializeField]
        private float rotationRate = 0.1f;

        [SerializeField]
        private float airControlRatio = 0.5f;

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _rotationComponent = GetComponent<RotationComponent>();
            _jumpComponent = GetComponent<JumpComponent>();
        }

        public void AddInput(Vector2 movementInput)
        {
            if (!_characterController)
                return;
            Vector3 forward = gameObject.transform.forward;
            Vector3 right = gameObject.transform.right;
            if (hasToRotateWithCamera)
            {
                Quaternion horizontalQuaternion =
                    _rotationComponent.GetHorizontalRotationQuaternion();
                forward = horizontalQuaternion * Vector3.forward;
                right = horizontalQuaternion * Vector3.left;
                if (characterMesh && movementInput.y > 0.0f)
                {
                    characterMesh.transform.rotation = Quaternion.Lerp(
                        characterMesh.transform.rotation,
                        horizontalQuaternion,
                        rotationRate
                    );
                }
            }
            forward *= movementInput.y;
            right *= -movementInput.x;
            Vector3 newSpeed = speed * (forward + right);
            if (_jumpComponent && _jumpComponent.IsJumping)
            {
                newSpeed *= Time.fixedDeltaTime * airControlRatio;
                newSpeed.y = _jumpComponent.CurrentUpSpeed;
                _characterController.Move(newSpeed);
                return;
            }
            _characterController.SimpleMove(newSpeed);
        }

        // yyip8
        public void SetPosition(Vector3 position)
        {
            _characterController.enabled = false;
            transform.position = position;
            _characterController.enabled = true;
        }
        // end yyip8
    }
}
