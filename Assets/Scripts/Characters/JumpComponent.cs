﻿using System;
using UnityEngine;

namespace Characters
{
    [RequireComponent(typeof(CharacterController))]
    public class JumpComponent : MonoBehaviour
    {

        private CharacterController _characterController;

        [SerializeField] private float jumpingForce = 50.0f;

        private float _currentUpSpeed = 0.0f;

        public float CurrentUpSpeed => _currentUpSpeed;

        private float _newUpSpeed = 0.0f;
        
        private bool _isJumping = false;

        public bool IsJumping => _isJumping;

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
        }

        private void FixedUpdate()
        {
            _isJumping = !_characterController.isGrounded;
            if (!_isJumping) return;
            _currentUpSpeed = _newUpSpeed * Time.fixedDeltaTime;
            _newUpSpeed += Physics.gravity.y * Time.fixedDeltaTime;
        }

        public void Jump()
        {
            if (!_characterController.isGrounded || _isJumping) return;
            _isJumping = true;
            _currentUpSpeed = jumpingForce * Time.fixedDeltaTime;
            _newUpSpeed = jumpingForce;
        }
    }
}